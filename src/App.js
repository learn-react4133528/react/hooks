import {LessonWrapper} from "./layout/LessonWrapper";
import {UseState} from "./Use State/UseState";
import {UseEffect} from "./Use Effect/UseEffect";
import IntervalClassCounter from "./Use Effect/Counter";
import {IntervalHookCounter} from "./Use Effect/IntervalHookCounter";
import {DataFetching} from "./Use Effect/DataFetching";
import {FocusInput} from "./UseRef/Focusinput";

function App() {
  return (
    <div className="App">
        <LessonWrapper lesson={1}>
            <UseState />
            <UseEffect />
        </LessonWrapper>
        <LessonWrapper lesson={2}>
            <IntervalClassCounter />
            <IntervalHookCounter />
            <DataFetching />
            <FocusInput />
        </LessonWrapper>
    </div>
  );
}

export default App;
