export const LessonWrapper = ({lesson, children}) => (
    <div className={'lesson'}>
        <h3>Lesson {lesson}</h3>
        <>{children}</>
    </div>
);
