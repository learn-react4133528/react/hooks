import { useState } from "react";

export const UseState = () => {
    const [count, setCount] = useState(0);
    const [name, setName] = useState({
        firstName:'',
        lastName: ''
    });
    const [items, setItems] = useState([]);
    


    const handleClick = () => {
        //Don`t call inside loops, conditions, or nested functions
        // Call only in React component
        // setCount(count + 1)
        // prev
        setCount(prevCount => prevCount + 1)
    }

    const addItem = () => {
        setItems([...items, {
            id: items.length,
            value: Math.floor(Math.random() * 10) + 1
        }])
    }

    return (
        <>
            <button onClick={handleClick}>{count}</button>

            <input type={'text'} value={name.firstName} onChange={e => setName({...name, firstName: e.target.value})} />
            <input type={'text'} value={name.lastName} onChange={e => setName({...name, lastName: e.target.value})} />
            <h2>First name - {name.firstName}</h2>
            <h2>First name - {name.lastName}</h2>
            <h2>{JSON.stringify(name)}</h2>
            <button onClick={addItem}>Add a number</button>
            <ul>
                {
                    items.map((item) => <li key={item.id}>{item.value}</li>)
                }
            </ul>
        </>
    )
}

