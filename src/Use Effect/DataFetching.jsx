import {useEffect, useState} from "react";
import axios from "axios";

export const DataFetching = () => {
    // const [posts, setPosts] = useState([]);
    const [post, setPost] = useState({});
    const [id, setId] = useState(1);


    useEffect(() => {
        // axios.get('https://jsonplaceholder.typicode.com/posts')
        //     .then(res => {
        //         console.log(res)
        //         setPosts(res.data)
        //     })
        //     .catch((err) => {
        //         console.log(err)
        //     })

        axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`)
            .then(res => {
                console.log(res)
                setPost(res.data)
            })
            .catch((err) => {
                console.log(err)
            })
    }, [id])

    return (
        <div>
            <input type={'text'} value={id} onChange={e => setId(e.target.value)}/>
            <div>{post.title}</div>
            {/*{*/}
            {/*    posts.map(post => <li key={post.id}>{post.title}</li>)*/}
            {/*}*/}
        </div>
    )
};