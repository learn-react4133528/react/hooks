import React, {useEffect, useState} from 'react';

export const MouseMove = () => {
    const [x, setX] = useState(0);
    const [y, setY] = useState(0);

    //componentDidMount
    useEffect(() => {
        console.log('componentDidMount')
        window.addEventListener('mousemove', logMousePosition)
        return () => {
            window.removeEventListener('mousemove', logMousePosition)
        }
    }, [])

    const logMousePosition = (e) => {
        console.log('Mouse move')
        setX(e.clientX)
        setY(e.clientY)
    }
    return (
        <div>
            Hooks X - {x} Y - {y}
        </div>
    )
};