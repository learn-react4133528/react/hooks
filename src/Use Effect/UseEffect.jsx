import React, {useEffect, useState} from 'react';
import {MouseMove} from "./MouseMove";

export const UseEffect = () => {
    const [count, setCount] = useState(0);
    const [name, setName] = useState('');
    const [display, setDisplay] = useState(true);

    //componentDidUpdate
    // useEffect(() => {
    //     console.log('Update')
    //     document.title = `You clicked ${count} times`
    // })
    //componentDidUpdate with condition
    useEffect(() => {
        console.log('update with condition')
        document.title = `You clicked ${count} times`
    }, [count])

    return (
        <div>
            <button onClick={() => setCount(count + 1)}>{count}</button>
            <input type="text" value={name} onChange={(e) => setName(e.target.value)}/>

            <button onClick={() => setDisplay(!display)}>Toggle display</button>
            {display && <MouseMove />}
        </div>
    )
};